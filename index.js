const http = require('http')

const NAME = process.env['npm_package_name'] || 'app-name'
const VERSION = process.env['npm_package_version'] || '0.0.1'
const PORT = parseInt(process.env['PORT']) || 3000
const HOST = process.env['HOST'] || '0.0.0.0'
const NODE_ENV = process.env['NODE_ENV'] || 'development'

const server = http.createServer((req, res) => {
  res.statusCode = 200
  res.setHeader('Content-Type', 'application/json')
  res.end(JSON.stringify({ hello: 'world' }))
})

server.listen(PORT, HOST)

server.on('listening', () => {
  if (NODE_ENV === 'development') {
    console.log(`${NAME}:${VERSION} listening at http://${HOST}:${PORT}/`)
  }
})
