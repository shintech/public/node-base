const path = require('path')

module.exports = {
  testPathIgnorePatterns: ['<rootDir>/.next/', '<rootDir>/node_modules/'],
  rootDir: path.join('..', '..'),
  setupFiles: ['<rootDir>/config/jest/jest.setup.js'],
  testEnvironment: 'node',
  snapshotSerializers: ['enzyme-to-json/serializer'],
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga|ico)$': '<rootDir>/config/jest/fileMock.js'
  }
}
