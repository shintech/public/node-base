# shintech/public/node-base

## Table of Contents
1. [ Synopsis ](#synopsis)
2. [ Usage ](#usage) <br />
	a. [Development ](#development) <br />
	b. [Production ](#production) <br />
	c. [git hooks ](#git-hooks)

<a name="synopsis"></a>
### Synopsis

Next.JS
  
<a name="install"></a>
### Installation/Configuration

    npm install
    
    # or
    
    yarn install

<a name="usage"></a>
### Usage
<a name="development"></a>
#### Development

    npm run dev
    
    # or
    
    yarn dev

<a name="production"></a>
#### Production
    docker-compose build && docker-compose up -d
    
<a name="git-hooks"></a>
#### git hooks
    ln -s /path/to/repo/config/hooks/hook /path/to/repo/.git/hooks/
